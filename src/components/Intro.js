import React, { Component } from 'react'

class Intro extends Component {
  render() {
    return (
      <div id='intro'>
        <div>
          <h1>Jack Maunders</h1>
          <h2>Web Developer</h2>
          <div>
            <p>Hi, I’m a Web Developer who enjoys creating responsive websites &amp; useful web apps. Do you have a new challenge for
					  me? Take a look at a few of my projects below and get in touch!</p>
            <a className='contact-me-top' id='contact-btn' href='#contact'>Contact Me</a>
          </div>
        </div>
      </div>
    )
  }
}

export default Intro