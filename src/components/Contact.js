import React, { Component } from 'react'
import Sending from '../images/sending.gif'

class Contact extends Component {
  constructor () {
    super()
    this.state = {
      emailInputFilled: false
    }
    this.emailInputChange = this.emailInputChange.bind(this)
  }

  emailInputChange (e) {
    if (e.target.value) this.setState({ emailInputFilled: true })
    else this.setState({ emailInputFilled: false })
  }

  render () {
    return (
      <div id='contact'>
        <div className='contact-message'>
          <h2>Contact Me</h2>
          <h3>Want to get in touch or talk to me about a project?</h3>
          <h3>Fill in the form and lets talk.</h3>
        </div>
        <form className='contact-form' method='POST' action='https://formspree.io/me@jackmaunders.xyz'>
          <fieldset>
            <input type='text' name='name' autoComplete='name' required />
            <label className='input-label' htmlFor='name'>Your Name</label>
          </fieldset>
          <fieldset>
            <input 
              type='email'
              name='email'
              autoComplete='email'
              required
              className={this.state.emailInputFilled ? 'isFilled' : ''} 
              onChange={this.emailInputChange} />
            <label className='input-label' htmlFor='email'>Your Email</label>
          </fieldset>
          <fieldset>
            <textarea name='message' rows='7' required></textarea>
            <label className='textarea-label' htmlFor='message'>Your Message</label>
          </fieldset>
          <fieldset>
            <input type='submit' name='submit' value='Send Message' />
          </fieldset>
        </form>
        <div className='contact-img'>
          <img src={Sending} alt='paper plane' />
        </div>
      </div>
    )
  }
}

export default Contact