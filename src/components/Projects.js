import React, { Component } from 'react'
import Project from './Project'
import ProjectData from '../data/projects.json'

class Projects extends Component {
  render() {
    return (
      <div id='projects'>
        {ProjectData.map(project => <Project data={project} key={project.Title} />)}
      </div>
    )
  }
}

export default Projects