import React, { Component } from 'react'
import Helmet from 'react-helmet'

import './main.css'

class Layout extends Component {
  render() {
    return (
      <div>
        <Helmet
          title={'Jack Maunders || Web Developer'}
        >
          <html lang='en' />
        </Helmet>
        {this.props.children}
      </div>
    )
  }
}

export default Layout