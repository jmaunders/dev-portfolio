import React from 'react'
import Layout from '../components/Layout'
import Intro from '../components/Intro'
import Projects from '../components/Projects'
import Contact from '../components/Contact'
import Footer from '../components/Footer'

const IndexPage = () => (
  <Layout>
    <Intro />
    <Projects />
    <Contact />
    <Footer />
  </Layout>
)

export default IndexPage